package com.jwind.core;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jwind.netease.HttpPost;
import com.jwind.utils.FileReadAndWrite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: J.wind
 * Date: 2018/7/25
 * Time: 3:00 PM
 * 按照用户ID进行歌单查询，歌单详情，歌曲下载。 -- 调用python脚本版
 */
public class StartSpider {

    public static void main(String[] args) {
        try {
            //第一步：根据用户id获取清单
            String uid = FileReadAndWrite.readDataFromConsole("请输入用户id：");
            String userMusicList = HttpPost.getUserMusicList(uid);
            JSONObject json = JSONObject.parseObject(userMusicList);
            JSONArray playlist = json.getJSONArray("playlist");
            System.out.println("   歌单ID：            歌单名称： ");
            for (Object play : playlist) {
                JSONObject ids = JSONObject.parseObject(play.toString());
                String id = ids.getString("id");
                String name = ids.getString("name");
                System.out.println(id + "    " + name);
            }
            //第二步：获取用户每个歌单的详情
            String playId = FileReadAndWrite.readDataFromConsole("请输入歌单ID：");
            String rest = HttpPost.getPlayList(playId);
            JSONObject jsonObject = JSONObject.parseObject(rest);
            JSONObject playLists = jsonObject.getJSONObject("playlist");
            JSONArray tracks = playLists.getJSONArray("tracks");
            List<String> nameList = new ArrayList<String>();
            System.out.println("歌曲名称：");
            for (Object track : tracks) {
                JSONObject name = JSONObject.parseObject(track.toString());
                System.out.println(name.getString("name"));
                nameList.add(name.getString("name"));
            }
            //第三步：写入文件列表
            String musicName = FileReadAndWrite.readDataFromConsole("请输入歌曲名称：(all：下载全部)");
            if ("all".equals(musicName) || "ALL".equals(musicName)) {
                for (String name : nameList) {
                    String path = FileReadAndWrite.pwd();
                    FileReadAndWrite.write(path + "/bin/music_list.txt", name);
                }
            } else {
                String[] names = musicName.split(",");
                for (String name : names) {
                    String path = FileReadAndWrite.pwd();
                    FileReadAndWrite.write(path + "/bin/music_list.txt", name);
                }
            }
            //第四步：下载文件
            System.out.println(FileReadAndWrite.python());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
