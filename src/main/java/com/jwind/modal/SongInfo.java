package com.jwind.modal;

import lombok.Data;

/**
 * Created by 姜振东
 * At 2018/10/24 4:25 PM
 * Email: jiangzd102@outlook.com
 */
@Data
public class SongInfo {
    private String id;
    private String name;
    private Integer br;
    private String url;
    private String type;
}
