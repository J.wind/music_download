package com.jwind.utils.robot;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;

import java.io.*;
/**
 * Created by 姜振东
 * At 2018/10/25 12:14 PM
 * Email: jiangzd102@outlook.com
 */
public class DownloadFile {
    // 构造存储的文件名称（去除url中不能作为文件名的字符）
    private String getFileName(String url, String contentType) {
        String fileName = "";
        if (contentType.contains("html"))
            fileName = url.replaceAll("[\\?/:*|<>\"]", "_") + ".html";
        else
            // 将类似text/html的字符串截取出文件类型后缀
            fileName = url.replaceAll("[\\?/:*|<>\"]", "_") + "."
                    + contentType.substring(contentType.indexOf("/") + 1);
        return fileName;
    }

    // 一个写文件的方法
    private void saveToLocal(byte[] content, String filePath) {
        try {
            DataOutputStream out = new DataOutputStream(new FileOutputStream(
                    new File(filePath)));
            for (int i = 0; i < content.length; i++) {
                try {
                    out.write(content[i]);
                } catch (IOException e) {
                    System.err.println("write error");
                    e.printStackTrace();
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    System.err.println("close error");
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("out error");
            e.printStackTrace();
        }
    }

    // 将page的内容写入文件系统
    public String downloadFile(String url) {
        String fileName = "";
        HttpClient httpClient = new HttpClient();
        // 设置HttpClient的连接管理对象，设置 HTTP连接超时5s
        httpClient.getHttpConnectionManager().getParams()
                .setConnectionTimeout(5000);
        if (url.indexOf("http") != -1) {
            GetMethod getMethod = new GetMethod(url);
            // 作用是什么？
            getMethod.getParams().setParameter(HttpMethodParams.SO_TIMEOUT,
                    5000);
            getMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
                    new DefaultHttpMethodRetryHandler());
            try {

                int statut = httpClient.executeMethod(getMethod);
                if (statut != HttpStatus.SC_OK)
                    System.err.println("Method failed: "
                            + getMethod.getStatusLine());
                else {
                    byte[] content = getMethod.getResponseBody();
                    fileName = "e:\\temp\\"
                            + getFileName(url,
                            getMethod.getResponseHeader("Content-Type")
                                    .getValue());
                    saveToLocal(content, fileName);
                    System.out.println(url + "::" + fileName);
                }
            } catch (HttpException e) {
                System.err.println("getmethod error");
                e.printStackTrace();
            } catch (IOException e) {
                System.err.println("io error");
                e.printStackTrace();
            }
        }
        return fileName;
    }
}
